with open('in2.txt', 'w') as f:
    curUrl = ""
    curPath = ""
    for i in range(1, 10000):
        char = chr(ord('a') + (i % 25))
        curUrl += char
        curPath += char
        if i % 25 == 2:
            curUrl += "." 
            curPath += "/" 
        f.write("https://{url}/{path}\n".format(
                    url = curUrl,
                    path = curPath))
